<?php
// Define API host URL
const HOST = "https://articles.gfaure.eu";

// Describe database connection
const DB = [
  'name' => 'articles',
  'password' => 'password',
  'host' => '127.0.0.1',
  'type' => 'mysql',
];

// Describe resources with their expected
// fields in URIs & associations with other resources
// primary key defaults to id, foreign keys defaults to []
// columns (returned by get & allowed to update) defaults to *
const RESOURCES = [
  'article' => [
    'fields' => ['id'],
    'foreign_keys' => ['user'],
  ],
  'opinion' => [
    'fields' => ['positive'],
    'foreign_keys' => ['article', 'user'],
    'primary_key' => 'article,user',
  ],
  'user' => [
    'fields' => ['id'],
    'columns' => 'id,role,username',
  ],
];

// Virtual resources that support only GET,
// used to query meta information
// Currently only support COUNT & LAST
const VIRTUAL_RESOURCES = ['COUNT', 'LAST'];

// Describe permissions per resource per user role
/* Description of rights
 * GET : query
 * POST : create
 * PATCH : partial update
 * PUT : complete (only) update
 * DELETE : delete
 * COUNT : query the total number of
 * 
 * prepend any field (except COUNT) with a ?
 * to allow this method only on resources
 * that have a "user" field associated with 
 * the user primary key
 */
const PERMISSIONS = [
  'article' => [
    'anonymous' => ['GET', 'COUNT'],
    'moderator' => ['GET', 'COUNT', 'DELETE'],
    'publisher' => ['POST', 'GET', 'COUNT', '?PUT', '?PATCH', '?DELETE'],
  ],
  'opinion' => [
    'anonymous' => ['COUNT'],
    'moderator' => ['GET', 'COUNT'],
    'publisher' => ['POST', '?GET', 'COUNT', '?PUT', '?DELETE'],
  ],
  'user' => [
    'anonymous' => ['COUNT'],
    'moderator' => ['GET', 'COUNT'],
    'publisher' => ['?GET', 'COUNT'],
  ],
];
