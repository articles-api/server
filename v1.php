<?php
require_once 'base.php';
// Instantiate the db connexion with CRUD operations
require_once 'Crud.php';
$db = new Crud(DB, RESOURCES, VIRTUAL_RESOURCES);

// Store query string in array
$query = $db->parse_query($_GET['query']);
// Store asked ressource (& virtual resource) in strings
$resource = $db->get_resource($query);
$virtual_resource = $db->get_virtual_resource($query);

// No authentication in v0.1
// Defaults user to id=1 as we dont authenticate
if (!isset($body['user']))
  $body['user'] = 1;
if ($virtual_resource !== false) {
  $virtual_resource .= ' of ';
} else {
  $virtual_resource = '';
}

$unknown_method = '';

// Deliver proper response to client according to HTTP method
switch ($method) {
  case 'POST':
    try {
      deliver_response(201, "Successfully created $resource", $db->post($body, $query));
    } catch (Exception $e) {
      deliver_response($e->getCode(), "Failed to create $resource", $e->getMessage());
    }
    break;
  case 'PATCH':
    try {
      deliver_response(200, "Successfully patched $resource", $db->patch($body, $query));
    } catch (Exception $e) {
      deliver_response($e->getCode(), "Failed to patch $resource", $e->getMessage());
    }
    break;
  case 'PUT':
    try {
      deliver_response(200, "Successfully updated $resource", $db->put($body, $query));
    } catch (Exception $e) {
      deliver_response($e->getCode(), "Failed to update $resource", $e->getMessage());
    }
    break;
  case 'DELETE':
    try {
      deliver_response(200, "Successfully deleted $resource", $db->delete($query));
    } catch (Exception $e) {
      deliver_response($e->getCode(), "Failed to delete $resource", $e->getMessage());
    }
    break;
  default:
    $unknown_method = " (method $method unknown, fallback to GET)";
  case 'GET':
    try {
      deliver_response(200, "Sucessfully retrieved $virtual_resource$resource(s)$unknown_method", $db->get($query));
    } catch (Exception $e) {
      deliver_response($e->getCode(), "Failed to retreive $virtual_resource$resource(s)", $e->getMessage());
    }
}
