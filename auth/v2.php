<?php
require_once '../base.php';

if ($method != 'POST')
  deliver_response(405, "$method method is not supported by this authentication API", "only the POST method is supported to query a JWT");

if (!isset($body['username']) || !isset($body['password']))
  deliver_response(400, "Malformed query", "missing username or password");

try {
  deliver_response(201, "Successfully authenticated, here's the base64 encoded JSON web token", $auth->login($body['username'], $body['password']));
} catch (Exception $e) {
  deliver_response($e->getCode(), "Failed to authenticate", $e->getMessage());
}
