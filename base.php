<?php
// Import project configuration
require_once 'config.php';
// Import functions
require_once 'lib.php';
// Instantiate the authentication provider
require_once 'Auth.php';
$auth = new Auth(DB, PERMISSIONS);


// Set HTTP header to json
header('Content-Type:application/json');
// Store used HTTP method
$method = $_SERVER['REQUEST_METHOD'];
// Store body JSON in array
$body = json_array(file_get_contents('php://input'));
