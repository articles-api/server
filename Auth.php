<?php
// Utils used for authentication
require_once 'jwt_utils.php';

class Auth
{
  private PDO $PDO;
  private array $PERMISSIONS;
  private const JWT_HEADER = ['alg' => 'HS256', 'typ' => 'JWT'];
  private const SECRET = 'A real secret is better than a false one, even if it depends on what you consider being "better" in life. For example, one could say that PHP is better than C to develop backends, but a videogame backend certainly will be way more performant if written in C or C++ !';

  public function __construct(array $db, array $permissions)
  {
    $this->PERMISSIONS = $permissions;

    $this->PDO = new PDO(
      $db['type'] . ':host=' . $db['host'] . ';dbname='
        . $db['name'],
      isset($db['user']) ? $db['user'] : $db['name'],
      $db['password']
    );
  }

  public function login(string $username, string $password)
  {
    $req = $this->PDO->prepare('
      SELECT id,role,password FROM user WHERE username=:username
    ');
    $req->bindParam('username', $username);

    // Hide errors for security
    try {
      $req->execute();
    } catch (PDOException $e) {
      throw new Exception('internal database error', 500);
    }

    $res = $req->fetch();

    if ($res) {
      if (password_verify($password, $res['password'])) {
        return generate_jwt(
          self::JWT_HEADER,
          [
            'id' => $res['id'],
            'username' => $username,
            'role' => $res['role'],
            'exp' => time() + 900,
          ],
          self::SECRET
        );
      }
    }

    throw new Exception('wrong credentials', 401);
  }

  public function authorize(string $method, string $resource, string $where = null)
  {
    // Translate virtual ressource in real method
    if ($method == 'LAST')
      $method = 'GET';

    if (isset($this->PERMISSIONS[$resource])) {
      $perms = $this->PERMISSIONS[$resource];
    } else {
      throw new Exception('either $resource dont exists or nothing is allowed with it', 404);
    }

    $raw_jwt = get_bearer_token();

    if ($raw_jwt !== false) {
      $jwt = is_jwt_valid($raw_jwt, self::SECRET);

      if ($jwt === false)
        throw new Exception("invalid authentication token, you have to authenticate properly before using $method on $resource", 401);

      if (!isset($jwt['role']))
        throw new Exception("invalid authentication token, missing role, you have to authenticate properly before using $method on $resource", 401);

      $role = $jwt['role'];
      $id = $jwt['id'];
    } else {
      $role = 'anonymous';
      $id = '';
    }

    if (isset($perms[$role])) {
      if (in_array($method, $perms[$role])) {
        return ['role' => $role, 'id' => $id];
      } else if (in_array('?' . $method, $perms[$role])) {
        // Hide errors for security
        try {
          $req = $this->PDO->query("SELECT user FROM $resource $where");
        } catch (PDOException $e) {
          throw new Exception('internal database error', 500);
        }

        if ($req->fetch()['user'] == $id)
          return ['role' => $role, 'id' => $id];
      }
    }

    if (strlen($id) == 0)
      $id = 'of unknown id';
    throw new Exception("$role user $id don't have the permission to $method $resource(s)", 403);
  }
}
