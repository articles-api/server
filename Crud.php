<?php
class Crud
{
  private PDO $PDO;
  private array $RESOURCES;
  private array $VIRTUAL_RESOURCES;

  public function __construct(array $db, array $resources, array $virtual_resources = ['COUNT', 'LAST'])
  {
    $this->RESOURCES = $resources;
    $this->VIRTUAL_RESOURCES = $virtual_resources;

    $this->PDO = new PDO(
      $db['type'] . ':host=' . $db['host'] . ';dbname='
        . $db['name'],
      isset($db['user']) ? $db['user'] : $db['name'],
      $db['password']
    );
  }

  // Parse query string into array of ressources
  // as associatives arrays with 'field' => 'value'
  public function parse_query(string $query_string)
  {
    // Sanitize extensively the query string as
    // we don’t use prepared queries to keep code simple
    $query = explode(
      '/',
      htmlentities(
        trim($query_string, "/ \n\r\t\v\x00")
      )
    );

    $parsed_query = [];
    for ($i = 0; $i < count($query); $i++) {
      // Get rid of trailing 's' as tables are named singular
      if (substr($query[$i], -1) == 's') {
        $resource = substr($query[$i], 0, -1);
      } else {
        $resource = $query[$i];
      }

      if (array_key_exists($resource, $this->RESOURCES)) {
        $parsed_query[$resource] = [];

        foreach ($this->RESOURCES[$resource]['fields'] as $field) {
          $i++;
          if (isset($query[$i])) {
            if (in_array(strtoupper($query[$i]), $this->VIRTUAL_RESOURCES)) {
              $parsed_query[$query[$i]] = [];
              break;
            }
            if ($query[$i] != '-') {
              $parsed_query[$resource] += [$field => $query[$i]];
            }
          }
        }
      } else if (in_array(strtoupper($resource), $this->VIRTUAL_RESOURCES)) {
        $parsed_query[$resource] = [];
      } else {
        throw new Exception("this API doesn't manage $resource(s)", 400);
      }
    }

    return array_reverse($parsed_query);
  }

  public function get_resource(array $query)
  {
    if (in_array(strtoupper(array_keys($query)[0]), $this->VIRTUAL_RESOURCES)) {
      return array_keys($query)[1];
    } else {
      return array_keys($query)[0];
    }
  }

  public function get_virtual_resource(array $query)
  {
    if (in_array(strtoupper(array_keys($query)[0]), $this->VIRTUAL_RESOURCES)) {
      return strtoupper(array_keys($query)[0]);
    } else {
      return false;
    }
  }

  private function is_primary_key(string $resource, string $field)
  {
    if (isset($this->RESOURCES[$resource]['primary_key'])) {
      if ($field == $this->RESOURCES[$resource]['primary_key'])
        return true;
    } else {
      if ($field == 'id')
        return true;
    }
    return false;
  }

  public function get_where(array $query)
  {
    $where = '';
    $foreign_keys = [];

    foreach ($query as $resource => $fields) {
      if (isset($this->RESOURCES[$resource]['foreign_keys'])) {
        foreach ($this->RESOURCES[$resource]['foreign_keys'] as $key) {
          $foreign_keys[$key] = $resource;
        }
      }
      foreach ($fields as $field => $value) {
        if (isset($foreign_keys[$resource]) && $this->is_primary_key($resource, $field)) {
          $foreign = $foreign_keys[$resource];
          $where .= "  AND $foreign.$resource=$value";
        } else {
          $where .= "  AND $resource.$field=$value";
        }
      }
    }

    // Replace first "  AND" with WHERE
    if (strlen($where) > 3)
      $where = substr_replace($where, 'WHERE', 0, 5);

    return $where . ' ';
  }

  private function get_join(array $query_input)
  {
    $query = $query_input;
    $join = '';

    foreach ($query as $resource => $content) {
      // Delete foreign keys as they dont need to be joined
      if (isset($this->RESOURCES[$resource]['foreign_keys'])) {
        foreach ($this->RESOURCES[$resource]['foreign_keys'] as $key) {
          try {
            unset($query[$key]);
          } catch (Exception $e) {
            // Do nothing, errors are expected to happen
          }
        }
      }

      // If there is still a resource after
      // and possibility to join it, JOIN it
      if (next($query) !== false) {
        $next_resource = key($query);
        if (isset($this->RESOURCES[$next_resource]['foreign_keys'])) {
          if (in_array($resource, $this->RESOURCES[$next_resource]['foreign_keys'])) {
            $join .= "JOIN $next_resource ON $resource.id = $next_resource.$resource ";
          }
        }
      }
    }

    return $join;
  }

  private function get_fields_and_values(array $body, array $query)
  {
    $fields_output = [];
    $values_output = [];
    $currentResource = $this->get_resource($query);

    // Sanitize the fields and values as
    // we don’t use prepared queries to keep code simple
    foreach ($body as $field => $value) {
      if (isset($field)) {
        array_push($fields_output, htmlentities(trim($field, "/ '\\\"\n\r\t\v\x00")));
        array_push($values_output, htmlentities(trim($value, "'\\\"\t\v\x00")));
      }
    }
    foreach ($query as $resource => $fields) {
      if ($resource != $currentResource) {
        foreach ($fields as $value) {
          if (isset($field)) {
            array_push($fields_output, htmlentities(trim($resource, "/ '\\\"\n\r\t\v\x00")));
            array_push($values_output, htmlentities(trim($value, "/ '\\\"\n\r\t\v\x00")));
            break;
          }
        }
      }
    }

    return [$fields_output, $values_output];
  }

  public function post(array $body, array $query)
  {
    $fields_values = $this->get_fields_and_values($body, $query);
    $values = '';
    $resource = $this->get_resource($query);
    $req = 'INSERT INTO ' . $resource . ' (';

    for ($i = 0; $i < count($fields_values[0]); $i++) {
      $req .= $fields_values[0][$i] . ',';
      $values .= $fields_values[1][$i] . ',';
    }

    $req = substr_replace($req, ')', -1); // Replace , with )
    $req .= ' VALUES (';

    // WARNING We insert directly the value here,
    // maybe it poses a security threat,
    // even if we sanitized the string before
    foreach ($fields_values[1] as $value) {
      $req .= "'$value',";
    }

    $req = substr_replace($req, ')', -1); // Replace , with )

    // var_dump($req);
    if (!$this->PDO->query($req))
      throw new Exception("internal error, database request failed", 500);

    // Add LAST to query to only return just inserted resource
    return $this->get(['LAST' => [], $resource => []]);
  }

  public function get(array $query)
  {
    $resource = $this->get_resource($query);
    $virtual_resource = $this->get_virtual_resource($query);
    $order_by = '';
    $columns = '*';

    if (isset($this->RESOURCES[$resource]['columns']))
      $columns = $this->RESOURCES[$resource]['columns'];

    // Handle virtual resources that support only GET,
    // used to query meta information
    switch ($virtual_resource) {
      case 'COUNT':
        $req = "SELECT COUNT(*) count FROM $resource ";
        break;
      case 'LAST':
        if (isset($this->RESOURCES[$resource]['primary_key'])) {
          $pk = $this->RESOURCES[$resource]['primary_key'];
        } else {
          $pk = 'id';
        }
        $order_by = "ORDER BY $pk DESC LIMIT 1 ";
        $req = "SELECT $resource.$columns FROM $resource ";
        break;
    }
    if (!$virtual_resource)
      $req = "SELECT $resource.$columns FROM $resource ";

    $req .= $this->get_join($query);
    $req .= $this->get_where($query);
    $req .= $order_by;

    // var_dump($req);
    try {
      $exec = $this->PDO->query($req);
    } catch (PDOException $e) {
      if ($e->getCode() == '42S22')
        throw new Exception("innexistant resource or field", 404);
      throw new Exception("internal error, database request failed", 500);
    }
    if (!$exec)
      throw new Exception("internal error, database request failed", 500);

    $data = $exec->fetchAll(PDO::FETCH_ASSOC);
    if (count($data) == 0)
      throw new Exception("no $resource found with this query", 404);
    // Return data
    return $data;
  }

  public function patch(array $body, array $query)
  {
    $fields_values = $this->get_fields_and_values($body, $query);
    $req = 'UPDATE ' . array_keys($query)[0] . ' SET ';

    for ($i = 0; $i < count($fields_values[0]); $i++) {
      $req .= $fields_values[0][$i] . '=\'' . $fields_values[1][$i] . '\',';
    }

    $req = substr_replace($req, ' ', -1); // Replace , with space
    $req .= $this->get_where($query);

    if (!$this->PDO->query($req))
      throw new Exception("internal error, database request failed", 500);

    return $this->get($query);
  }

  public function put(array $body, array $query)
  {
    // TODO verify that resource is transmitted
    // entirely & throw an error if not

    return $this->patch($body, $query);
  }

  public function delete(array $query)
  {
    $resource = $this->get_resource($query);
    $id = isset($query[$resource]['id']) ? ' of id=' . $query[$resource]['id'] : '';

    $row = $this->get($query);
    if ($row === false)
      throw new Exception("$resource$id not found", 404);

    $req = 'DELETE FROM ' . array_keys($query)[0] . ' ';
    $req .= $this->get_where($query);

    if (!$this->PDO->query($req))
      throw new Exception("internal error, database request failed", 500);

    return $row;
  }
}
