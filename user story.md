# Pour un utilisateur non authentifié :

__En tant__ qu'utilisateur non authentifié, __je veux__ consulter les articles existants __afin de__ pouvoir en prendre connaissance.

__En tant__ qu'utilisateur non authentifié, __je veux__ consulter le nombre d’avis positif sur un article existant __afin de__ savoir s’il je devrais le lire.  
(Il n’est pas précisé dans la consigne que les utilisateurs puissent avoir accès à cette information, mais cela nous semblait pertinent)

# Pour un utilisateur authentifié avec le rôle publisher :

__En tant__ qu'utilisateur authentifié avec le rôle publisher, __je veux__ poster un nouvel article __afin de__ le partager avec la communauté.

__En tant__ qu'utilisateur authentifié avec le rôle publisher, __je veux__ consulter mes propres articles __afin de__ pouvoir les modifier ou les supprimer si nécessaire.

__En tant__ qu'utilisateur authentifié avec le rôle publisher, __je veux__ consulter les articles publiés par les autres utilisateurs __afin de__ découvrir de nouveaux contenus.

__En tant__ qu'utilisateur authentifié avec le rôle publisher, __je veux__ modifier les articles dont je suis l'auteur __afin de__ les mettre à jour si nécessaire.

__En tant__ qu'utilisateur authentifié avec le rôle publisher, __je veux__ supprimer les articles dont je suis l'auteur __afin de__ retirer les contenus obsolètes.

__En tant__ qu'utilisateur authentifié avec le rôle publisher, __je veux__ liker/disliker les articles publiés par les autres utilisateurs __afin de__ donner mon avis sur les contenus proposés.

# Pour un utilisateur authentifié avec le rôle moderator :

__En tant__ qu'utilisateur authentifié avec le rôle moderator, __je veux__ consulter n'importe quel article __afin d'avoir__ une vue d'ensemble des contenus disponibles.

__En tant__ qu'utilisateur authentifié avec le rôle moderator, __je veux__ supprimer n'importe quel article __afin de__ retirer les contenus inappropriés ou contraires aux règles de la plateforme.
