<?php
// Define HTTP response delivering function
function deliver_response($status = 200, $status_message = null, $data = null)
{
  // Set HTTP header with status
  header("HTTP/1.1 $status $status_message");
  // header("Access-Control-Allow-Origin: " . HOST);
  header("Access-Control-Allow-Origin: *");

  // Set HTTP response body with message & data
  $response['status'] = $status;
  if (isset($status_message))
    $response['status_message'] = $status_message;
  if (isset($data))
    $response['data'] = $data;

  // var_dump($response);
  // Return the response as JSON & end the script
  die(json_encode($response));
}

// Convert JSON string to an associative array
function json_array(string $json_string)
{
  return json_decode($json_string, true);
}
