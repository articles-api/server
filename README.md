# Articles API implementation

Core of the Articles API, written in PHP.

Essentially, CRUD operations on 3 resources :
- articles
- opinions (Likes/Dislikes)
- users

By users authenticated via JWTs that can have 2 different roles, granting them different permissions :
- moderator
- publisher

Or users can also be anonymous, in such case the request operations are generally the only allowed.

## Versions

There are currently two versions of the API. The main difference between is the authentication. The **v1** doesn’t do any authentication and allows any operation. The **v2** do authentication (through `auth/v2`) and respect permissions described below.

Both are referenced as "vX" in the specs below.

## Crud class

The core of the API is the DbCrud class, which is basically a RESTful URI + JSON body to SQL database query translator.
The class was made as generic as possible, and should be easy to use with others database schemes by just changing the configuration.

Database scheme specific configuration is made through some arrays passed to the class constructor (defined in the config.php file).

### Configuration of resources

Resources are configured in a 2D, associative array which associates **table** names with **fields** that will be looked for in the URI
or used to *join* the table with another if needed.
**Fields** usually represents **columns** or SQL statements that will be used as *WHERE* conditions,
or **foreign keys** that will be used in *join on* conditions.

### Configuration of permissions

Permissions are configured in a 3D, associative array which associates **resources** with **roles** with **operations** users that have this role are allowed to perform.

## Resources & associated allowed operations per user role

Every operation that is not explicitly defined below should be denied.

### (All) **Articles**

`vX/articles`

#### Everyone

Request (GET)

```json
{
  "data":[
  {
    "content":"LARGE STRING",
    "id":"INTEGER",
    "publication_date":"DATE",
    "title":"SMALL STRING",
    "user":"INTEGER (ID of user)"
  },
  {…},
  …
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

> Maybe rename "user" in "author" in this JSON

#### Publishers

Create (POST)

```json
{
  "content":"LARGE STRING",
  "title":"SMALL STRING",
  "publication_date":"DATE (defaults to today)"
  "user":"USER (publisher) ID checked against JWT"
}
```

The user (author) is set to himself.

### (All) Articles published by a specific user of identifier $U

`vX/users/$U/articles`

#### Everyone

Request (GET)

```json
{
  "data":[
  {
    "content":"LARGE STRING",
    "id":"INTEGER",
    "publication_date":"DATE",
    "title":"SMALL STRING",
    "user":"SMALL STRING"
  },
  {…},
  …
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

### (All) Articles liked by a specific user of identifier $U

`vX/users/$U/opinions/1/articles`

#### Moderators & Publishers

Request (GET)

```json
{
  "data":[
  {
    "content":"LARGE STRING",
    "id":"INTEGER",
    "publication_date":"DATE",
    "title":"SMALL STRING",
    "user":"SMALL STRING"
  },
  {…},
  …
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

### (All) Articles disliked by a specific user of identifier $U

`vX/users/$U/opinions/0/articles`

#### Moderators & Publishers

Request (GET)

```json
{
  "data":[
  {
    "content":"LARGE STRING",
    "id":"INTEGER",
    "publication_date":"DATE",
    "title":"SMALL STRING",
    "user":"SMALL STRING"
  },
  {…},
  …
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

### (All) Articles to which a specific user reacted (either liked or disliked) of identifier $U

`vX/users/$U/opinions/-/articles`

#### Moderators & Publishers

Request (GET)

```json
{
  "data":[
  {
    "content":"LARGE STRING",
    "id":"INTEGER",
    "publication_date":"DATE",
    "title":"SMALL STRING",
    "user":"SMALL STRING"
  },
  {…},
  …
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

### (A specific) Article of identifier $A

`vX/articles/$A`

#### Everyone

Request (GET)

```json
{
  "data":[
  {
    "content":"LARGE STRING",
    "id":"INTEGER",
    "publication_date":"DATE",
    "title":"SMALL STRING",
    "user":"SMALL STRING"
  }
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

#### Moderators

Delete (DELETE) the article of identifier $A

#### Publisher of which identifier is equal to the article author’s identifier (the author of the article)

Delete (DELETE) the article of identifier $A

Update (PATCH or PUT) the article of identifier $A 

```json
{
  "title":"SMALL STRING",
  "content":"LARGE STRING"
}
```

### (All) **Opinions** (Likes/Dislikes)

`vX/opinions`

#### Moderators

Request (GET)

```json
{
  "data":[
  {
    "article":"INTEGER (id of article)",
    "like":"BOOLEAN",
    "user":"INTEGER (id of user)",
  },
  {…},
  …
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

### (All) Opinions published by a specific user of identifier $U

`vX/users/$U/opinions`

#### Moderators

Request (GET)

```json
{
  "data":[
  {
    "article":"INTEGER (id of article)",
    "like":"BOOLEAN",
    "user":"INTEGER (id of user)",
  },
  {…},
  …
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

### (All) Opinions published on a specific article of identifier $A

`vX/articles/$A/opinions`

#### Moderators

Request (GET)

```json
{
  "data":[
  {
    "article":"INTEGER (id of article)",
    "like":"BOOLEAN",
    "user":"INTEGER (id of user)",
  },
  {…},
  …
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

#### Publishers of which identifier is **not** equal to the article author’s identifier (**others** than the author of the article)

Create (POST)

```json
{
  "like":"BOOLEAN"
}
```

User is set to this publisher identifier

### (A specific) Opinion published on a specific article of identifier $A by a specific user of identifier $U

`vX/articles/$A/users/$U/opinions`

#### Moderators

Request (GET)

```json
{
  "data":[
  {
    "article":"INTEGER (id of article)",
    "like":"BOOLEAN",
    "user":"INTEGER (id of user)",
  }
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

#### Publishers of which identifier is equal to the user’s opinion (publisher of the opinion)

Update (PATCH)

```json
{
  "like":"BOOLEAN"
}
```

### (All) **Users**

`vX/users`

**Currently, there is no operation allowed with all users.**

The users are created & modified by the database admin via SQL.

### User(s) that published a specific article of identifier $A

`vX/articles/$A/users`

#### Everyone

Request (GET)

```json
{
  "data":[
  {
    "id":"INTEGER (id)",
    "role":"SMALL STRING",
    "username":"SMALL STRING",
  },
  {…},
  …
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

### (All) Users that reacted (1:liked, 0:disliked, *-*:both) to a specific article of identifier $A

`vX/articles/$A/opinions/[0|1|-]/users`

#### Moderators

Request (GET)

```json
{
  "data":[
  {
    "id":"INTEGER (id)",
    "role":"SMALL STRING",
    "username":"SMALL STRING",
  },
  {…},
  …
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

### (A specific) User of identifier $U

`vX/users/$U`

#### Moderators & Publishers

Request (GET)
```json
{
  "data":[
  {
    "id":"INTEGER (id)",
    "role":"SMALL STRING",
    "username":"SMALL STRING",
  }
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

### **Total number** of reactions (1:liked, 0:disliked)

`vX/opinions/[0|1]/COUNT`
`vX/opinions/COUNT`

#### Moderators & Publishers

Request (GET)
```json
{
  "data":[
    {
      "count":"INTEGER",
    },
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

### Total number of reactions (1:liked, 0:disliked) to a specific article of identifier $A

`vX/articles/$A/opinions/[0|1]/COUNT`
`vX/articles/$A/opinions/COUNT`

#### Moderators & Publishers

Request (GET)
```json
{
  "data":[
    {
      "count":"INTEGER",
    },
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

### Total number of reactions (1:liked, 0:disliked) from a specific user of identifier $U

`vX/users/$U/opinions/[0|1]/COUNT`
`vX/users/$U/opinions/COUNT`

#### Moderators & Publishers

Request (GET)
```json
{
  "data":[
    {
      "count":"INTEGER",
    },
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

### Total number of articles

`vX/articles/COUNT`

#### Moderators & Publishers

Request (GET)
```json
{
  "data":[
    {
      "count":"INTEGER",
    },
  ],
  "status":"HTTP STATUS CODE",
  "status_message":"STATUS MESSAGE"
}
```

## Authentication

Authentication is done through a JWT requested by a `POST` request at the auth API.

`auth/vX` (X >= 2)

This request looks for two fields, `username` & `password`.

### Request a new authentication web token (`POST`)

```json
{
  "username":"SMALL STRING",
  "password":"SMALL STRING"
}
```

#### In case of correct credentials

```json
{
  "status": 201,
  "status_message": "Successfully authenticated, transfering the JWT",
  "data": "BASE64 STRING (JWT)"
}
```

#### In case of wrong credentials

```json
{
  "status": 401,
  "status_message": "Failed to authenticate",
  "data": "wrong credentials"
}
```

### JWT (decoded from base64)

#### Header

```json
{
  "alg":"HS256",
  "typ":"JWT"
}
```

#### Body

```json
{
  "username":"SMALL STRING",
  "role":"SMALL STRING",
  "exp":"DATE"
}
```
